import React from "react";
import Navbar from "./pages/Navbar";
import Landing from "./pages/Landing";
import About from "./pages/About";
import Projects from "./pages/Project";
import Blog from "./pages/Blog";
import Contact from "./pages/Contact";

function Index() {
  return (
    <>
      <div className="container">
        <div>
          <Navbar />
        </div>
        
        <div id="Home">
          <Landing />
        </div>
        <div id="About">
          <About />
        </div>
        <div id="Projects">
          <Projects />
        </div>
        <div id="Blog">
          <Blog/>
        </div>
        <div id="Contact">
          <Contact/>
        </div>

      </div>
     
    </>
  );
}

export default Index;
