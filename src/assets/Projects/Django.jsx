import React from 'react';
import Card from "./Card";
import axios from "axios";

function Django() {
  const [data, setData] = React.useState([]);
  const getData = async () => {
    const res = await axios.get(`http://localhost:5000/api/project`);
    const data = res.data.data;
    const reactData = data.filter((react)=>react.Technology[0].Backend === "Django") 
    setData(reactData);
  };
  React.useEffect(() => {
    getData();
  }, []);
  return (
    <div className="Wrapper">
      {data.map((data) => (
        <Card
          Head={data.ProjectTitle}
          image={data.Project[0].image}
          description={data.Project[0].description}
          url={data.Project[0].url}
          Frontend={data.Technology[0].Frontend}
          Backend={data.Technology[0].Backend}
          Database={data.Technology[0].Database}
        />
      ))}
    </div>
  )
}

export default Django