import React,{useState,useEffect} from "react";
import Card from "./Card";
import axios from "axios";

function All() {
  const [data, setData] = useState([]);

  const getData = async () => {
    const res = await axios.get(`http://localhost:5000/api/project`);
    const data = res.data.data;
    setData(data);
  };
  useEffect(() => {
    getData();
  }, []);
  return (
    <div className="Wrapper">
      {data.map((data) => (
        <Card
          Head={data.ProjectTitle}
          image={data.Project[0].image}
          description={data.Project[0].description}
          url={data.Project[0].url}
          Frontend={data.Technology[0].Frontend}
          Backend={data.Technology[0].Backend}
          Database={data.Technology[0].Database}
        />
      ))}
    </div>
  );
}

export default All;
