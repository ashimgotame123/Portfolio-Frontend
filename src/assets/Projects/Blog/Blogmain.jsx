import React from "react";
import BlogCard from "./BlogCard";
import image from "D:/React Learning/Ashim/frontend/src/assets/images/1.jpg";
import image2 from "D:/React Learning/Ashim/frontend/src/assets/images/2.jpg";
import image3 from "D:/React Learning/Ashim/frontend/src/assets/images/expi.png";

export default function componentName() {
  return (
    <div className="Blog">
      <BlogCard
        slug="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,"
        title="All i need is Just a little patience"
        img={image}
        category="Politics"
        date="12 Dec 2021"
      />
      <BlogCard
        slug="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,"
        title="Mama just Killed a man, put a gun against his head"
        img={image2}
        category="Technology"
        date="1 jan 2022"
      />
      <BlogCard
        slug="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,"
        title="Buddy you are a mess, A bid disgrace "
        img={image3}
        category="Education"
        date="12 Nov 2022"
      />
    </div>
  );
}
