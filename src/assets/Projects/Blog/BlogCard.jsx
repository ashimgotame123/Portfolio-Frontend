import React from "react";
import "./Blog.css";
import {Link} from 'react-router-dom';
import {BsArrowRight} from 'react-icons/all'

function BlogCard(props) {
  return (
    <div className="blogcard">
      <img src={props.img} alt="blog" />
      <div className="Blogdata">
      <h5>{props.category}</h5>
      <h3>{props.title}</h3>
      <h4>{props.slug}</h4>
      <h6>{props.date}</h6>
      <Link to='/'><button>Read More...</button></Link>

      </div>
    </div>
  );
}

export default BlogCard;
