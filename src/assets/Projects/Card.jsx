import React from 'react';
import './Card.css';

function Card(props) {
  return (
        <div className="card">
            <img src={props.image} alt="img" />
            <div className="info">
                <h4>{props.Head}</h4>
                <p>{props.description}</p>
               <h6> Technologies Used :<br/> {props.Frontend},  {props.Backend}, {props.Database} <br/> <br/></h6>
                <a href={props.url}><button>View Code</button></a>   
                <h1></h1> 
            </div>
        </div>
  )
}

export default Card