import * as React from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import All from "../Projects/All";
import Django from "../Projects/Django";
import Mern from "../Projects/Mern";
import Rproject from "../Projects/Rproject";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function Projects(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <>
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    </>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function BasicTabs() {
  const [value, setValue] = React.useState(0);

  const handleChange = (_event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: "100%" }}>
      <Box
        sx={{ borderBottom: 1, borderColor: "divider" }}
        className="Project-items"
      >
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          <Tab label="All" className="btnn" {...a11yProps(0)} />
          <Tab label="React JS" className="btnn" {...a11yProps(1)} />
          <Tab label="Python Django" className="btnn" {...a11yProps(2)} />
          <Tab label="MERN Stack" className="btnn" {...a11yProps(3)} />
        </Tabs>
      </Box>
      <Projects value={value} index={0}>
        <All />
      </Projects>
      <Projects value={value} index={1}>
        <Rproject />
      </Projects>
      <Projects value={value} index={2}>
        <Django />
      </Projects>
      <Projects value={value} index={3}>
        <Mern />
      </Projects>
    </Box>
  );
}
