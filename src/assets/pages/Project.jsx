import React from "react";
import Projects from "./Projects";

function Project() {
  return (
    <div className="Projects">
      <h1>Projects</h1>
      <div className="hello">
        <h3>Latest Works</h3>
      </div>
      <Projects />
    </div>
  );
}

export default Project;
