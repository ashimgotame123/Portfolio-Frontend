import React from "react";
import { Box, Button } from "@mui/material";
import image from "../images/v.png";

function Landing() {
  return (
    <div className="Landing">
      <Box className="Naming">
        <h3 className="animate__animated animate__lightSpeedInLeft">
          Ashim Gotame
          <br /> <span>Mern Stack Developer</span>
        </h3>
        <p className="animate__animated animate__lightSpeedInRight">
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries,
        </p>
        <a href="#Projects">
          <button className="button animate__animated animate__backInUp">
            View my Work
          </button>
        </a>
        <a href="#Contact">
          <button className="button animate__animated animate__backInUp">
            Hire Me !!!
          </button>
        </a>
      </Box>
      <Box className="Image animate__animated animate__backInUp">
        <img src={image} />
      </Box>
    </div>
  );
}

export default Landing;
