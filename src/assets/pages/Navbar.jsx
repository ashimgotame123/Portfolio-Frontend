import React from "react";
import { AppBar, Typography, Button, Box } from "@mui/material";

import '../Style.css'

function Navbar() {
  return (
    <AppBar position="static" className="Navbar" id="Header">
      <Typography className="Navbar-title">Ashim Gotame</Typography>
      <Box className="Nav-items animate__animated animate__backInRight">
      
      <a href="#Home"><Button className="btn">Home</Button></a>
      <a href="#About"> <Button className="btn">About </Button></a>
      <a href="#Projects"> <Button className="btn">Projects</Button></a>
      <a href="#Blog"> <Button className="btn">Blog</Button></a>
      <a href="#Contact"> <Button className="btn">Contact</Button></a>
      

      </Box>
    </AppBar>
  );
}

export default Navbar;
