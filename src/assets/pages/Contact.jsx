import React from "react";
import { Button } from "@mui/material";
import contact from "../images/Con.png";
import call from '../images/call.png'
import email from '../images/email.png'
import loc from '../images/loc.png'


function Contact() {
  return (
    <div className="Projects">
      <h1>Contact Me</h1>
      <div className="Middle">
        <h6>Have a Question? Get in Touch !</h6>
        <div className="Middle-cont">
          <div className="contact-top">
            <img src={call} alt="img" />
            <h4>Phone</h4>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s,
            </p>
            <h5>+977 9861886603</h5>
          </div>
          <div className="contact-top">
            <img src={email} alt="img" />
            <h4>Email</h4>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s,
            </p>
            <h5>Ashimgotame123@gmail.com</h5>

          </div>
          <div className="contact-top">
            <img src={loc} alt="img" />
            <h4>Location</h4>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s,
            </p>
            <h5>Kalanki, Kathmandu</h5>

          </div>
        </div>
      </div>

      <div className="Contact">
        <div className="Touch">
          <img src={contact} alt="contact" />
        </div>
         
        <form className="contact-form">
        <label>Get in Touch</label>
          <input placeholder="  Name" className="Name" />
          <input placeholder="  Email" type="email" className="Name" />
          <textarea placeholder=" message" className="Name Message" />
          <Button className=" submit"> Get In Touch</Button>
        </form>
      </div>
    </div>
  );
}

export default Contact;
