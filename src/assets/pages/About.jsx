import React from 'react';
import edu from '../images/Edu.png'
import Skill from '../images/Ski.png'
import Hob from '../images/Hob.png'
import Exp from '../images/Exp.png'
import Per from '../images/14.png'
import {Link} from 'react-router-dom'






function About() {
  return (
    <div className='Projects'>
      <small><span>01. </span> Know about me</small>
      <h1> About me</h1>
      <div className="Grid">
        <div className='one'>
        <img src={Per} alt='img'/>
          <h3>Ashim Gotame</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ut congue nunc, malesuada fringilla ligula. Etiam commodo lacinia est ullamcorper ullamcorper. Donec at magna libero. Morbi facilisis risus dignissim augue tristique commodo. Suspendisse porta id quam vel feugiat. Mauris tempus.</p>
        <Link to='/asa' className='Link' >See More ...</Link>
        </div>
        <div className='two'>
        <img src={edu} alt='img'/>
          <h3>Education</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ut congue nunc, malesuada fringilla ligula. Etiam commodo lacinia est ullamcorper ullamcorper. Donec at magna libero. Morbi facilisis risus dignissim augue tristique commodo. Suspendisse porta id quam vel feugiat. Mauris tempus.</p>
          <Link to='/' className='Link' >See More ...</Link>
        </div>
        <div className='three'>
        <img src={Skill} alt='img'/>
        <h3>Skills</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ut congue nunc, malesuada fringilla ligula. Etiam commodo lacinia est ullamcorper ullamcorper. Donec at magna libero. Morbi facilisis risus dignissim augue tristique commodo. Suspendisse porta id quam vel feugiat. Mauris tempus.</p>
          <Link to='/' className='Link' >See More ...</Link>
         </div>
        <div className='four'>
        <img src={Hob} alt='img'/>
        <h3>Hobbies</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ut congue nunc, malesuada fringilla ligula. Etiam commodo lacinia est ullamcorper ullamcorper. Donec at magna libero. Morbi facilisis risus dignissim augue tristique commodo. Suspendisse porta id quam vel feugiat. Mauris tempus.</p>
          <Link to='/' className='Link' >See More ...</Link>
        </div>
        <div className='five'>
        <img src={Exp} alt='img'/>
        <h3>Experiences</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ut congue nunc, malesuada fringilla ligula. Etiam commodo lacinia est ullamcorper ullamcorper. Donec at magna libero. Morbi facilisis risus dignissim augue tristique commodo. Suspendisse porta id quam vel feugiat. Mauris tempus.</p>
          <Link to='/'  className='Link'>See More ...</Link>
        </div>

      </div>
    </div>
  )
}

export default About;