
import React from 'react';
import {BrowserRouter as Router , Routes ,Route} from 'react-router-dom'
import Index from './assets/Index';
import Homepage from '../src/Admin/DashBoard/Homepage'
import Login from '../src/Admin/Auth/Login';
import { useSelector } from "react-redux";



function App() {
  const isLoggedIn = useSelector((state) => state.isLoggedIn);
  return (
    <>
     <Router>
      <Routes>
      <Route path='/' element={<Index/>}/>
       {isLoggedIn && <Route path='/Admin/' element={<Homepage/>}/>} 
       {!isLoggedIn && <Route path='/Admin/' element={<Login/>}/>}
      </Routes>
    </Router>
   
    </>
  )
}

export default App