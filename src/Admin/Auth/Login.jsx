import React from "react";
import "./Login.css";
import { Link, useNavigate } from "react-router-dom";
import user from '/src/assets/images/User.png'
import {AiOutlineMail,RiLockPasswordLine} from 'react-icons/all';
import axios from 'axios';
import { useDispatch } from "react-redux";
import { authActions } from "D:/React Learning/Ashim/frontend/src/store";
import {toast} from 'react-toastify'

function Login() {
  const history = useNavigate();
  const dispatch = useDispatch();
  const [input, setInput] = React.useState({
    email: "",
    password: "",
  });
  const sendRequest = async () => {
    const res = await axios.post(`http://localhost:5000/auth/login`,{
      email: input.email,
      password: input.password,
    })
    const data = await res.data;
    localStorage.setItem('token',res.data.Token);
    return data;
  };
  const handleChange = (e) => {
    setInput((prev) => ({
      ...prev,
      [e.target.name]: [e.target.value],
    }));
  };
  const hamdleSubmit = (e) => {
    e.preventDefault();
    sendRequest()
    .then(() => dispatch(authActions.login()))
    .then(() => history("/Admin/"))
    .then(()=>toast.success("Welcome"))
    .catch(()=>toast.error("Incorrect Credentials"))
  };
  return (
    <div className="Login-Main">
      <form onSubmit={hamdleSubmit} className="Log-tab">
      <div className="box">
      <img src={user}  alt='user'/>
        <h1>Login</h1>
        <small>Enter your credentials</small>
        <div className="Tab">
          <label><AiOutlineMail/> </label>
          <input
            type="email"
            placeholder="   Email"
            name="email"
            value={input.email}
            onChange={handleChange}
            required
          />
        </div>
        <div className="Tab">
          <label><RiLockPasswordLine/>  </label>
          <input
            type="password"
            placeholder="   Password"
            name="password"
            value={input.password}
            onChange={handleChange}
            required
          />
        </div>
          <button className="but" type="submit">Login</button>
        </div>
      </form>
    </div>
  );
}

export default Login;
